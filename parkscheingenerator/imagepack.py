from os import listdir
from PIL import Image, ImageDraw

parkscheindir = "./parkscheine"
dinA4constraints = ('DINA4', 1710, 2418)


def printSizeOfImages(images):
    for image in images:
        loadedImage = Image.open(image)
        width, height = loadedImage.size
        print(f'Breite: {width}, Höhe: {height}')


def getContentsFromDirectory(dir):
    contents = listdir(dir)
    return list(map(lambda x: parkscheindir + '/'+x, contents))


def getNextBoundaries(currentBox, documentConstraints, imageSize):
    """
    Rechnet die nächsten Koordinaten für die Positionierung eines neues Bildes aus
    """
    cBw, cBh = currentBox
    iSw, iSh = imageSize
    dinFormat, dCw, dCh = documentConstraints

    # Auf die nächste Seite springen
    if cBh + iSh + iSh > dCh and cBw + iSw + iSw > dCw:
        return ('newpage', (0, 0))

    # Ein Zeile weiter springen
    if cBh + iSh + iSh <= dCh and cBw + iSw <= dCw:
        return ('currentpage', (cBw, cBh + iSh))

    # Zur nächsten Spalte springen
    if cBh + iSh + iSh > dCh and cBw + iSw + iSw <= dCw:
        return ('currentpage', (cBw+iSw, 0))
    else:
        return ('newpage', (0, 0))


def createDINA4PaperFromImages(images, documentConstraints):
    dinFormat, width, height = documentConstraints
    print('Merging images together')
    print(f'{dinFormat} -  width: {width}, heigth: {height} ')

    currentBox = (0, 0)

    documents = []
    numberOfImages = len(images)
    document = Image.new('RGB', (width, height), color='white')

    status = "currentpage"

    for index, image in enumerate(images):
        currentImage = Image.open(image)

        if status == 'currentpage':
            document.paste(currentImage, currentBox)
            status, currentBox = getNextBoundaries(currentBox, documentConstraints,
                                                   currentImage.size)

        if status == 'newpage':
            currentBox = (0, 0)
            documents.append(document)
            document = Image.new('RGB', (width, height), color='white')
            status = 'currentpage'
            currentBox = (0, 0)

    return documents


def runImagePack():
    images = getContentsFromDirectory(parkscheindir)
    docs = createDINA4PaperFromImages(images, dinA4constraints)
    for index, image in enumerate(docs):
        image.save(f'./merged/merge0{index}.png')
