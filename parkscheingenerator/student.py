from names import get_first_name, get_last_name
from PIL import Image, ImageDraw, ImageFont
import json


class Student:
    vorname = ""
    nachname = ""
    parkscheinnummer = ""
    verifikationnummer = ""
    studentindex = 0

    def __init__(self, index=0, parkscheinnummer=0, verifikationnummer=""):
        self.studentindex = index
        self.parkscheinnummer = str(parkscheinnummer)
        self.verifikationnummer = verifikationnummer
        self.vorname = get_first_name()
        self.nachname = get_last_name()

    def createSign(self):
        image = Image.open('./mockdata/parkschein.png')
        # initialise the drawing context with
        # the image object as background

        color = 'rgb(0, 0, 0)'  # white color
        draw = ImageDraw.Draw(image)
        # desired size
        font = ImageFont.truetype('./fonts/Verdana.ttf', size=55)
        # Draw Number
        (x, y) = (147, 460)
        draw.text((x, y), self.parkscheinnummer, fill=color, font=font)

        # Draw verification
        (x, y) = (195, 738)
        font = ImageFont.truetype('./fonts/Verdana.ttf', size=18)
        draw.text((x, y), self.verifikationnummer, fill=color, font=font)
        # save the edited image
        imagePath = "parkscheine/studenten_parkschein_{}.png"
        image.save(imagePath.format(self.studentindex),
                   optimize=True, quality=20)

    def __str__(self):
        return f'Vorname: {self.vorname}\nNachname: {self.nachname}\nVerifikationsnummer: {self.verifikationnummer}\nParkscheinnummer: {self.parkscheinnummer}\n'

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)
