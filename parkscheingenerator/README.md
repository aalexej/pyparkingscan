# parkscheingenerator (Python 3)

## Zusammenfassung

Generiert zufällig Parkausweise mit Vor- und Nachnamen, Parkausweisnummer und Verifikationsnummer.

| Ordner      | Inhalt                                        |
| ----------- | --------------------------------------------- |
| fonts       | Schrift für die Parkausweise                  |
| json        | JSON Datei mit allen Studenten                |
| merged      | zusammengefügte Parkausweise auf einem DIN-A4 |
| mockdata    | Muster für ein Parkauseis                     |
| parkscheine | generierte Parkausweise                       |

## Verwendete Bibliotheken

- PIL

- jsontools

- imagepack
