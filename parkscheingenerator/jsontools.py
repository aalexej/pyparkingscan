from student import Student


def writeJSONtoFile(jsonText):
    text_file = open("./json/students.json", "w")
    n = text_file.write(jsonText)
    text_file.close()


def createJSONFromStudents(students):
    jsonText = "["

    for student in students:
        jsonText += student.toJSON() + ',\n'

    jsonText += "]"
    return jsonText
