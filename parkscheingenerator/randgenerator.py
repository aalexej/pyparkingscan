from random import randint, choice
import string
from student import Student


def randSignNumber(numberLength=10):
    """Generates a random number of fixed length"""
    range_start = 10**(numberLength-1)
    range_end = (10**numberLength)-1
    return randint(range_start, range_end)


def randVerificationNumber(stringLength=12):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(choice(letters) for i in range(stringLength)).upper()


def randomStudents(numberOfStudents):
    """Return a list of studentobjects"""
    studentNumber = 0
    students = []
    while (studentNumber < numberOfStudents):
        student = Student(studentNumber, randSignNumber(),
                          randVerificationNumber())
        studentNumber = studentNumber + 1
        students.append(student)

    return students
