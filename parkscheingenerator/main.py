from randgenerator import randomStudents
from student import Student
from jsontools import createJSONFromStudents, writeJSONtoFile
from imagepack import runImagePack
from PIL import Image


def createImagesFromStudents(students):
    for student in students:
        student.createSign()


def createJSONFileFromStudents(students):
    jsonText = createJSONFromStudents(students)
    writeJSONtoFile(jsonText)


def createTestData(numberOfStudents):
    students = randomStudents(numberOfStudents)
    createJSONFileFromStudents(students)
    createImagesFromStudents(students)


if __name__ == "__main__":
    # createTestData(16)
    runImagePack()
