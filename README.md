# Parkausweisscanner mit OpenCV und Tesseract

| Ordner              | Inhalt                                           |
|:-------------------:|:------------------------------------------------ |
| DOCS                | Dokumentation und Folien von der Präsentation    |
| parkscheingenerator | Python-Programm zum generieren von Parkausweisen |
| pyParcingscan       | Python-Programm zum Scannen von Parkauseisen     |

```bash
# pyParcingscan usage: 

main.py [-h] [--camera Kameraquelle] [--videoinput <nr>]

Parkausweisscanner mit OpenCV und Tesseract

optional arguments:
  -h, --help            show this help message and exit
  --camera Kameraquelle
                        Starte die Bildübertragung von der Kamera
  --videoinput <nr>     Starte die Bildübertragung aus Dateien
```



## Weitere Links:

<div>
<a href="https://drive.google.com/open?id=1-pdJnqLbHaPelIY_PetFo03hKP1gdWIL">Vortragsfolien von Ray Smith über Tesseract OCR (Google Drive)</a>
</div>


