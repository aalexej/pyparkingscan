#!/usr/bin/env python
# -*- coding: utf-8 -*-
from time import sleep
import numpy as np
import cv2 as cv
import pytesseract
import canny
from parkschein import processTesseractText

videodirectory = '/home/andrei/Dokumente/Python/Bildverarbeitung_Projekt/pyParcingscan/videos/'


def doOCR(frame):
    """
    u'Führt eine Texterkennung durch
    """
    whitelistCharacters = '-c tessedit_char_whitelist=1234567890nrNR:'
    tessdata_dir_config = '--tessdata-dir "/home/andrei/Dokumente/Python/Bildverarbeitung_Projekt/pyParcingscan/tessdata_best"'
    sprache = 'deu'

    img_rgb = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
    ocrText = pytesseract.image_to_string(img_rgb,
                                          config=tessdata_dir_config,
                                          lang=sprache)

    processTesseractText(ocrText)


def drawBoxes(frame):
    """
    Zeichnet die Regionen um die gefundenen Zeichen ein
    """
    h, w, _ = frame.shape

    img_rgb = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
    boxes = pytesseract.image_to_boxes(img_rgb)

    # b ist eine Box
    for b in boxes.splitlines():
        b = b.split(' ')
        frame = cv.rectangle(frame, (int(b[1]), h - int(b[2])),
                             (int(b[3]), h - int(b[4])), (255, 0, 0), 1)
        cv.imshow('Boxes', frame)


def processVideo(videocapture, dim):
    ret, frame = videocapture.read()
    if ret:
        frame = cv.resize(frame, dim, interpolation=cv.INTER_AREA)
    else:
        # Video von vorne Beginnen
        videocapture.set(cv.CAP_PROP_POS_FRAMES, 0)
        _, frame = videocapture.read()

    return frame


def getScaleDimensions(frame):
    """
    Passt die größe für den Bildschirm an
    """
    w, h, _ = frame.shape
    if h >= 1080 or w >= 1920:
        return (h / 2, w / 2)

    return (w, h)


def runVideoCapture(fps=30, fromCamera=True, cameraSource=0, videoSource=1):
    # Kontrollvariable, zum Fortsetzen und Pausieren des Videos
    videoPlayback = True
    playbackSource = ''

    if fromCamera:
        playbackSource = cameraSource
    else:
        playbackSource = videodirectory + str(videoSource) + '.mp4'

    cap = cv.VideoCapture(playbackSource)

    _, frame = cap.read()

    dim = getScaleDimensions(frame)
    # Erstes Frame anzeigen

    if not fromCamera:
        cap.set(cv.CAP_PROP_FPS, 15)

    while (True):
        if videoPlayback:
            frame = processVideo(cap, dim)
            cv.imshow('Videoquelle', frame)

        key = cv.waitKey(20) & 0xFF
        # Schleife verlassen
        if key == ord('q'):
            break
        # Text aus einem Bild lesen
        if key == ord('t'):
            doOCR(frame)
        # regions of interest zeichnen
        if key == ord('b'):
            drawBoxes(frame)
        # Video anhalten oder fortsetzen
        if key == ord('p'):
            videoPlayback = not videoPlayback

    # When everything done, release the capture
    cap.release()
    cv.destroyAllWindows()
