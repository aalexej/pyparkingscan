import cv2 as cv

max_lowThreshold = 100
window_name = 'Edge Map'
title_trackbar = 'Min Threshold:'
ratio = 3
kernel_size = 3


def CannyThreshold(val, frame):
    low_threshold = val
    img_blur = cv.blur(frame, (3, 3))
    detected_edges = cv.Canny(img_blur, low_threshold, low_threshold * ratio,
                              kernel_size)
    mask = detected_edges != 0
    dst = frame * (mask[:, :, None].astype(frame.dtype))
    return dst