import cv2 as cv
import numpy as np
from matplotlib.pyplot import imshow, show


def matToString(mat):
    """
    Zeigt Anzahl der Zeilen, Spalte und Kanaele an
    :param mat: OpenCV Image
    :return:
    """
    rows, cols, channels = mat.shape
    #print(f'Zeilen: {rows}\nSpalten: {cols}\nKanaele: {channels}\n')


#Stellt ein Bild mit Matplotlib dar. Bild muss in BGR vorhanden sein
def showImageWithPlotlib(mat):
    rgbImage = cv.cvtColor(mat, cv.COLOR_BGR2RGB)
    pixels = np.array(rgbImage)
    imshow(pixels)
    show()


def showImageWithHighGUI(mat):
    cv.namedWindow('Bild', cv.WINDOW_GUI_EXPANDED)
    cv.imshow('Bild', mat)
    while True:
        keyevent = cv.waitKey(0) & 0xFF
        if keyevent == ord('q'):
            break
            cv.destroyAllWindows()