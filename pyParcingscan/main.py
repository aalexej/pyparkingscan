#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytesseract
from pydir import cvspecs as specs
from pydir import cvcamera as ocrcam
import cv2 as cv
import argparse


def createCLICommands():
    parser = argparse.ArgumentParser(
        description="Parkausweisscanner mit OpenCV und Tesseract")

    parser.add_argument('--camera',
                        help='Starte die Bildübertragung von der Kamera',
                        type=int,
                        default=0,
                        choices=[0, 2],
                        metavar='Kameraquelle')

    parser.add_argument('--videoinput',
                        help='Starte die Bildübertragung aus Dateien',
                        metavar='<nr>',
                        type=int,
                        choices=[1,2,3,4,5,6,7,8])

    return parser.parse_args()


if __name__ == "__main__":
    args = createCLICommands()

    if args.videoinput is None:
        ocrcam.runVideoCapture(fps=30,
                               fromCamera=True,
                               cameraSource=args.camera,
                               videoSource=13)
    else:
        ocrcam.runVideoCapture(fps=30,
                               fromCamera=False,
                               cameraSource=None,
                               videoSource=args.videoinput)
