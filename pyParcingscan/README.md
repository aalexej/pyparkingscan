# PyParcingscan (Python 2)

| Ordner        | Inhalt                                    |
| ------------- | ----------------------------------------- |
| bilder        | mustervorlage für einen leeren Parkschein |
| pydir         | verschiedene Skripte für GUI und OpenCV   |
| tessdata_best | Sprachdateien für Tesseract               |
| videos        | Videomaterial für Testzwecke              |



## Verwendete Bibliotheken

- opencv

- pytesseract

- numpy

- matplotlib

- re
