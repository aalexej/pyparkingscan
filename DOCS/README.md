# Bildverarbeitung Projekt

## Aktueller Stand 13.01.2019

![alt text](./res/ParkingScan.svg "UML Diagramm")

## Was wurde geschafft:

#### Python-Entwicklung

- Von einer internen oder externen Kamera wird ein Videostream geöffnet
- Es werden Regionen angezeigt, die wichtig für die Texterkennung sind
- Es wird eine Texterkennung ausgeführt, die einen Stringobjekt mit Text liefert
- Das Stringobjekt wird weiter verarbeitet und mit einem regulären Ausdruck der geforderte Inhalt extrahiert

###### Negatives:

Die Umgebenung unter Python 3.6 funktioniert nicht ordnungsgemäß. Deswegen wird für die Weiterentwicklung Python 2 genutzt.

#### Testdaten

- Für Testzwecke wurden mehrere Parkausweise mit unterschiedlichen Nummern generiert und eine einfache persistente Datenhaltung im JSON-Format erzeugt

```json
{
  "nachname": "Hopkins",
  "parkscheinnummer": "5761621288",
  "studentindex": 0,
  "verifikationnummer": "DXWAIOMCEQOA",
  "vorname": "David"
}
```

<p align="center">
<img style=" width: 75%" src="./res/hopkins.png">
</p>

## Weitere Vorgehensweise:

- weitere Tests müssen unter verschiedenen räumlichen Bedingungen ausgeführt werden und ggf. Anpassungen an der Vorverarbeitung des Bildes vorgenommen werden
- Die Validierung übereinstimmender Daten mit Name und Parkausweisnummer sollte ggf. noch implementiert werden

<p align="center">
<img style="width:100%;" src="./res/parkingscan.gif">
</p>

## Aktueller Stand, 07.01.2019

---

![alt text](./res/ParkingScan.png "UML Diagramm")

Stattdessen den Bilateral-Filter anwenden oder anisotropischer Filter

---

## Was wurde bisher geschafft:

#### App-Entwicklung

- Android/Java
  - OpenCV kann mithilfe einer C++ Bibliothek verwendet werden
  - Bild kann von der Kamera aufgenommen und angezeigt werden
  - Mithilfe von dem MSER-Detection Algorithmus werden auch Key-Features entdeckt

#### Probleme

Es gibt zur Zeit wenige Ressourcen darüber, wie man Tesseract OCR unter Java verwendet.

Allerdings gibt es eine GitHub Quelle mit Beispielcode, welcher funktionieren soll.

https://github.com/yushulx/android-tesseract-ocr

---

## Weitere Vorgehensweise:

Der Prototyp kann weiter mit Python entwickelt und getestet werden. Wenn die Anwendung funktioniert und richtige Ergebnise anzeigt, dann wird die bestehende Code-Basis in eine Java/Android -Anwendung überführt.

weitere Gründe, wieso es Sinn macht einen Prototypen mit Python zu entwickeln:

- Debuggen von Anwendungen unter Android-Studio nur bedingt möglich (Textausgabe mit Logs)
- der hohe Zeitaufwand ensteht wegen der Infrastruktur
  - Java Native Interface
    - OpenCV kann nicht als native Java-Library genutzt werden
    - Tesseract ebenfalls nicht
